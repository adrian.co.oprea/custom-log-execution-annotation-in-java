package com.dw.customlogexecution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StopWatch;

@SpringBootApplication
public class CustomLogExecutionApplication implements CommandLineRunner {

    @Autowired
    SomeService someService;

    public static void main(final String[] args) {
        SpringApplication.run(CustomLogExecutionApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        this.someService.someIntensiveWork();
    }
}
