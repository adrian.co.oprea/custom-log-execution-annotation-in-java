package com.dw.customlogexecution;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class LogExecutionAspect {
    @Pointcut("execution(@com.dw.customlogexecution.EnableLogging * *(..))")
    public void loggingEnabled() {
    }

    @Around("loggingEnabled()")
    public Object logExecutionTime(final ProceedingJoinPoint call) throws Throwable {
        final var watch = new StopWatch();

        watch.start();
        final var retVal = call.proceed();
        watch.stop();

        this.printMethodInformation(call, watch);

        return retVal;
    }

    private void printMethodInformation(final ProceedingJoinPoint call, final StopWatch watch) {
        final var annotationInstance = this.getAnnotationInstance(call);

        System.out.println(
            String.format(
                "Class: %s\nMethod: %s\nTime Spent in milliseconds: %s",
                call
                    .getTarget()
                    .getClass()
                    .getName(),
                call
                    .getSignature()
                    .getName(),
                annotationInstance.timeUnit() == TimeUnit.MILLISECONDS ?
                watch.getTotalTimeMillis() : watch.getTotalTimeSeconds()
            )
        );
    }

    private EnableLogging getAnnotationInstance(final ProceedingJoinPoint call) {
        final MethodSignature signature = (MethodSignature) call.getSignature();
        final Method method = signature.getMethod();

        return method.getAnnotation(EnableLogging.class);
    }

}
