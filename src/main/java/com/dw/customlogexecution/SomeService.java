package com.dw.customlogexecution;

import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class SomeService {
    @EnableLogging(timeUnit = TimeUnit.SECONDS)
    public void someIntensiveWork() throws InterruptedException {
        Thread.sleep(1000);
    }
}
